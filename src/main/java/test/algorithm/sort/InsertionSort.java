package test.algorithm.sort;

/**
 * @Projectname: algorithm-training-camp
 * @Filename: InsertionSort
 * @Author: FANSEA
 * @Date:2024/10/10 20:52
 */
public class InsertionSort {
    public static void main(String[] args){
        System.out.println(fun1());
    }
    
    public static String fun1(){
        try {
            System.out.println("A");
            return fun2();
        }finally {
            System.out.println("B");
        }
    }
    public static String fun2(){
        System.out.println("C");
        return "D";
    }
    public static void insertionSort(int[] arr){
        for(int i = 1; i < arr.length; i++){
            for (int j = i; j > 0; j--){
                if(arr[j] < arr[j-1]){
                    int temp = arr[j];
                    arr[j] = arr[j-1];
                    arr[j-1] = temp;
                }else{
                    break;
                }
            }
        }
    }
}
