package test.algorithm.dp;

import java.util.Scanner;

/**
 * 从左上角到右下角有多少种走法
 */
public class FindWay {
    static int[][] arr = new int [30][30];
    static int n;
    static int m;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        m = scanner.nextInt();
        System.out.println(dg(1, 1));
    }

    private static int dg(int i,int j) {
        if (i < 0 || i > n || j < 0 || j > m) {
            return 0;
        }
        // 如果行号，列号是偶数不能走
        if (i % 2 == 0 && j % 2 == 0) {
            return 0;
        }
        if (i == n && j == m) {
            return 1;
        }
        return dg(i,j+1) + dg(i+1,j);
    }




}
