package test.algorithm.dp;

/**
 * @Projectname: leetCode
 * @Filename: ClimbStairs
 * @Author: FANSEA
 * @Date:2024/9/10 17:09
 */

/**
 * 70. 爬楼梯
 */
public class ClimbStairs {
    public static void main(String[] args) {

    }
    public int climbStairs(int n) {
        if(n <= 2){
            return n;
        }
        return climbStairs(n-1) + climbStairs(n-2);
    }
    public int climbStairsPlus(int n) {
        if(n <= 2){
            return n;
        }
        int[] dp = new int[n+1];
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 3; i <= n; i++) {
            dp[i] = dp[i-1] + dp[i-2];
        }
        return dp[n];
    }
}
