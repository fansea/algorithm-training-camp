package test.algorithm.recursion;

import test.entity.ListNode;
import test.utils.ListNodeUtil;

/**
 * @Projectname: leetCode
 * @Filename: ReverseLinkedList
 * @Author: FANSEA
 * @Date:2024/9/11 9:04
 */
public class ReverseLinkedList {
    public static void main(String[] args) {
        ListNode head = ListNodeUtil.LIST_NODE_Lang;
        ListNodeUtil.printListNode(head);
        System.out.println();
        ListNode listNode = reverseList(head,null);
        System.out.println("-------------------------------------");
        ListNodeUtil.printListNode(listNode);
    }

    private static ListNode reverseList(ListNode head,ListNode pre) {
        Integer i = pre == null ? null : pre.val;
        if (head == null ) {
            return pre;
        }
        System.out.println("--------temp="+head.val+",pre="+i+"-----------------");
        if (head.next == null) {
            head.next = pre;
            System.out.println(head.val+ "->" + pre.val);
            return head;
        }
        ListNode next = head.next;
        ListNode temp = head.next.next;
        next.next = head;
        System.out.println(next.val+ "->" + head.val);
        head.next = pre;
        System.out.println(head.val+ "->pre");
        return reverseList(temp,next);
    }
}
