package test.algorithm.dfs;

import test.entity.TreeNode;

import static test.utils.TreeNodeUtil.arrayToTree;

/**
 * @Projectname: algorithm-training-camp
 * @Filename: HasPathSum
 * @Author: FANSEA
 * @Date:2024/9/19 19:51
 */
public class HasPathSum {
    public static void main(String[] args) {
        Integer[] arr = {5,4,8,11,null,13,4,7,2,null,null,null,1};
        TreeNode root = arrayToTree(arr);
        System.out.println(new HasPathSum().hasPathSum(root, 22));
    }
    public boolean hasPathSum(TreeNode root, int targetSum) {
        if (root==null){
            return false;
        }
        if (root.left==null&&root.right==null){
            return targetSum==root.val;
        }
        boolean b = hasPathSum(root.left, targetSum - root.val);
        boolean b1 = hasPathSum(root.right, targetSum - root.val);
        return b||b1;
    }

}
