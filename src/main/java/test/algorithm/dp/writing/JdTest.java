package test.algorithm.dp.writing;

import java.util.Scanner;

/**
 * @Projectname: algorithm-training-camp
 * @Filename: JdTest
 * @Author: FANSEA
 * @Date:2024/9/16 19:23
 */
public class JdTest {
    public static void main(String[] args) {
        // 创建一个用于接收用户输入的扫描器
        Scanner scanner = new Scanner(System.in);
        // 接收用户输入的第一个整数，表示矩阵的列数
        int n = scanner.nextInt();

        // 初始化一个二维数组，表示一个2行n列的矩阵
        int[][] a = new int[2][n];
        // 读取用户输入的矩阵元素
        for (int i = 0; i < 2; ++i) {
            for (int j = 0; j < n; ++j) {
                a[i][j] = scanner.nextInt();
            }
        }

        // 初始化两个二维数组，分别存储中间状态（dp）和临时结果（r）
        int[][] r = new int[2][n];
        int[][] dp = new int[2][n];

        // 设置边界条件
        dp[1][n - 1] = a[1][n - 1];
        dp[0][n - 1] = a[0][n - 1] + dp[1][n - 1];

        // 动态规划求解过程
        for (int j = n - 2; j >= 0; --j) { // 从倒数第二列到第一列
            for (int i = 0; i < 2; ++i) { // 对于每一行
                r[i][j] = dp[i][j + 1] + a[i][j]; // 计算当前位置加上从右侧一列过来的值
            }
            for (int i = 0; i < 2; ++i) { // 再次遍历每一行
                if ((i + j) % 2 != 0) { // 如果i+j为奇数，则选择最大值
                    dp[i][j] = Math.max(dp[i][j + 1], r[i ^ 1][j]) + a[i][j];
                } else { // 如果i+j为偶数，则选择最小值
                    dp[i][j] = Math.min(dp[i][j + 1], r[i ^ 1][j]) + a[i][j];
                }
            }
        }

        // 输出最终结果
        System.out.println(dp[0][0]);
    }
}
