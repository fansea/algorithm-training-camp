package test.algorithm.dp;

import java.util.Arrays;

/**
 * @Projectname: algorithm-training-camp
 * @Filename: LIS
 * @Author: FANSEA
 * @Date:2024/9/12 14:04
 */
public class LIS {

    public static void main(String[] args) {
        LIS lis = new LIS();
        int[] nums = {1,3,6,7,9,4,10,5,6};
        System.out.println(lis.lengthOfLIS(nums));
    }
    //最长递增子序列
    public int lengthOfLIS(int[] nums) {
        int[] dp = new int[nums.length];
        Arrays.fill(dp, 1);
        int maxCount = 1;
        for(int i=1;i<nums.length;i++){
            int max = 1;
            for(int j=0;j<i;j++){
                if(nums[i]>nums[j]){
                    dp[i] = Math.max(dp[j] + 1,max);
                    max = Math.max(dp[i],max);
                }
            }
            maxCount = Math.max(maxCount,max);
        }
        return maxCount;
    }
}
