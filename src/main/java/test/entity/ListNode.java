package test.entity;

/**
 * @Projectname: leetCode
 * @Filename: ListNode
 * @Author: FANSEA
 * @Data:2023/12/19 20:24
 */
public class ListNode {
    public int val;
    public ListNode next;
    ListNode() {}
    public ListNode(int val) { this.val = val; }
    public ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}
