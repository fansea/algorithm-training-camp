package test.utils;

import test.entity.ListNode;

/**
 * @Projectname: leetCode
 * @Filename: ListNodeConstants
 * @Author: FANSEA
 * @Date:2024/9/11 9:10
 */
public class ListNodeUtil {
    public static final ListNode LIST_NODE = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));

    public static final ListNode LIST_NODE_Lang = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5, new ListNode(6, new ListNode(7, new ListNode(8, new ListNode(9, new ListNode(10))))))))));

    // 遍历链表
    public static void printListNode(ListNode head) {
        while (head != null) {
            System.out.print(head.val + " ");
            head = head.next;
        }
    }

}
