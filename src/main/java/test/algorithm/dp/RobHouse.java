package test.algorithm.dp;


/**
 * @Projectname: leetCode
 * @Filename: RobHouse
 * @Author: FANSEA
 * @Date:2024/9/11 20:45
 */

/**
 * [198. 打家劫舍 - 力扣（LeetCode）](<a href="https://leetcode.cn/problems/house-robber/description/?envType=study-plan-v2&envId=dynamic-programming">...</a>)
 */
public class RobHouse {

    public static void main(String[] args) {
        int[] nums = {2,7,9,3,1};
        RobHouse robHouse = new RobHouse();
        System.out.println(robHouse.rob(nums));
    }
    public int rob(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        }
        if(nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        dp[1] = Math.max(nums[0], nums[1]);
        for (int i = 2; i < nums.length; i++) {
            dp[i] = Math.max(dp[i-2]+nums[i],dp[i-1]);
        }
        return dp[nums.length-1];
    }
}
