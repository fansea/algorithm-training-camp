package test.algorithm.dp;

import java.util.Scanner;

/**
 * @Projectname: leetCode
 * @Filename: FindWagDp
 * @Author: FANSEA
 * @Date:2024/9/10 23:25
 */
public class FindWagDp {
    static int[][] arr = new int [30][30];

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (i == 1 && j == 1) {
                    arr[i][j] = 1;
                    continue;
                }
                if (i%2==0 && j%2==0){
                    continue;
                }
                arr[i][j] = arr[i-1][j] + arr[i][j-1];
            }
        }
        System.out.println(arr[n][m]);
    }
}
