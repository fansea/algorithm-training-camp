package test.trueQuestion;

import test.entity.TreeNode;

import java.util.Stack;

/**
 * @Projectname: HandTornQuestions
 * @Filename: Shopee
 * @Author: FANSEA
 * @Date:2024/11/16 20:04
 */
public class Shopee {
    /*public static void main(String[] args) {
        int solution = pathSum(ROOT,"");
        System.out.println(solution);
    }*/
    public static void main(String[] args) {
        String str = "ab12ba";
        System.out.println(symmetric(str));
    }

    /**
     * 给你一个二叉树的根节点 root，树中每个节点都存放有一个0到9之间的数字。
     * 每条从根节点到叶节点的路径都代表一个数字: 例如，从根节点到叶节点的路径1->2 ->3 表示数字 123 。
     * 计算从根节点到叶节点生成的 所有数字之和，叶节点 是指没有子节点的节点。
     */
    public static int pathSum(TreeNode node,String value) {
        if (node == null) {
            return 0;
        }
        if(node.left == null && node.right == null) {
            System.out.println(value+node.val);
            return Integer.parseInt(value+node.val);
        }
        return pathSum(node.left,value+node.val) + pathSum(node.right,value+node.val);
    }

    public static boolean symmetric(String str) {
        int l = str.length();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < l / 2; i++) {
            stack.push(str.charAt(i));
        }
        if (l%2==0){
            for (int i = l / 2; i < l; i++) {
                if (stack.pop() != str.charAt(i)) {
                    return false;
                }
            }
        }else {
            for (int i = l / 2 + 1; i < l; i++) {
                if (stack.pop() != str.charAt(i)) {
                    return false;
                }
            }

        }
        return true;

    }

    public static int binarySearch(int[] arr, int target) {
        int l = 0;
        int r = arr.length - 1;
        while (l <= r) {
            int mid = l + (r - l) / 2;
            if (arr[mid] == target) {
                return mid;
            } else if (arr[mid] < target) {
                l = mid + 1;
            }else {
                r = mid - 1;
            }
        }
        return -1;
    }

}
