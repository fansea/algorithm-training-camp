package test.algorithm.recursion;

import test.entity.TreeNode;
import test.utils.TreeNodeUtil;

/**
 * @Projectname: algorithm-training-camp
 * @Filename: TreeDeepth
 * @Author: FANSEA
 * @Date:2024/9/13 18:15
 */
public class TreeDepth {
    public static TreeNode root = TreeNodeUtil.createSimpleBinaryTree();
    public static void main(String[] args) {
        TreeDepth treeDepth = new TreeDepth();
        System.out.println(treeDepth.treeDepth(root));
    }

    public int treeDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.max(treeDepth(root.left), treeDepth(root.right)) + 1;
    }
}
