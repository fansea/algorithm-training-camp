package test.entity;

/**
 * @Projectname: leetCode
 * @Filename: Singleton
 * @Author: FANSEA
 * @Data:2024/3/3 22:01
 */
public class Singleton {
    private static Singleton singleton;

    public Singleton() {
    }

    public static Singleton getInstance(){
        if (singleton == null){
            synchronized (Singleton.class){
                singleton = new Singleton();
            }
        }
        return singleton;
    }
}
